/** @type {import("prettier").Config} */
const config = {
    printWidth: 80,
    tabWidth: 4,
    useTabs: false,
    semi: true,
    singleQuote: false,
    trailingComma: "all",
    bracketSpacing: true,
    bracketSameLine: false,
    endOfLine: "lf",
    plugins: [
        "prettier-plugin-astro",
        "prettier-plugin-tailwindcss",
    ],
    astroAllowShorthand: true,
};

module.exports = config;
