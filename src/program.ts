export const PROGRAM = [
    {
        date: "2024-10-09",
        name: "Amerikanischer Krimi-Film aus dem Jahr 2005",
        originalVoice: true,
    },
    {
        date: "2024-10-23",
        name: "Herbig Animationsfilm aus dem Jahr 2007",
        originalVoice: false,
    },
    {
        date: "2024-10-30",
        name: "Spionage Action-Film Komödie aus dem Jahr 2014",
        originalVoice: true,
    },
    {
        date: "2024-11-06",
        name: "UNI4MIND Spezial 1",
        comment: "Filmstart: 19:30 Uhr",
        originalVoice: true,
    },
    {
        date: "2024-11-07",
        name: "UNI4MIND Spezial 2",
        comment: "Filmstart: 19:30 Uhr",
        originalVoice: false,
    },
    {
        date: "2024-11-20",
        name: "Ghibli Marathon",
        originalVoice: false,
    },
    {
        date: "2024-12-04",
        name: "Filmveranstaltung mit Feuerzangenbolen- und Glühweinverkauf",
        comment: "Ticketvorverkauf!",
        originalVoice: false,
    },
    {
        date: "2024-12-18",
        name: "US-Amerikanischer Science-Fiction Marathon aus dem Jahr 1982 und 2017",
        originalVoice: true,
    },
    {
        date: "2025-01-08",
        name: "Science-Fiction Action Thriller aus dem Jahr 2020",
        originalVoice: true,
    },
].map((item) => ({
    ...item,
    viewDate: new Date(item.date).toLocaleDateString("de", {day: "2-digit", month: "2-digit", year: "numeric"}),
}));

